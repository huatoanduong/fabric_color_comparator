﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCC.Model
{
    public class ColorRGB
    {
        public Color Color { get; set; }

        public double Red { get; set; }
        public double Green { get; set; }
        public double Blue { get; set; }

        public ColorRGB (Color color)
        {
            this.Color = color;

            this.Red = color.R;
            this.Green = color.G;
            this.Blue = color.B;
        }
    }
}
