﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FCC.Model
{
    public class ColorHSV
    {
        public Color Color { get; set; }

        public double Hue { get; set; }
        public double Saturation { get; set; }
        public double Value { get; set; }

        public ColorHSV(Color color)
        {
            this.Color = color;

            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            this.Hue = color.GetHue();
            this.Saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            this.Value = max / 255d;
        }

        public static ColorDiffRange operator - (ColorHSV color1, ColorHSV color2)
        {
            return new ColorDiffRange();
        }
    }
}