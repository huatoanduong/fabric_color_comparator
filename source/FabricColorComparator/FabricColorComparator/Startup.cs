﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FabricColorComparator.Startup))]
namespace FabricColorComparator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
